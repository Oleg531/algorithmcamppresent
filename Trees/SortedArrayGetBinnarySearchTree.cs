﻿namespace Trees
{
    using System;
    using System.Diagnostics;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class SortedArrayGetBinnarySearchTree
    {
        [TestMethod]
        public void When_SortedArrayProvided_ShouldCreate ()
        {
            var a = new int[] {10, 20, 25, 30, 33, 35, 40, 43, 45, 46, 50, 55, 60, 70};
            var root = CreateBstFromArray(a, 0, a.Length - 1);
            root.PrintFromRoot();
        }


        public Node CreateBstFromArray(int[] a, int startIndex, int endIndex)
        {
            var rootIndex = endIndex > startIndex ? (int) Math.Ceiling((endIndex - startIndex)/2d) + startIndex : startIndex;
            var rootValue = a[rootIndex];
            var root = new Node(rootValue)
            {
                Left = rootIndex > startIndex ? CreateBstFromArray(a, startIndex, rootIndex - 1) : null,
                Right = rootIndex < endIndex ? CreateBstFromArray(a, rootIndex + 1, endIndex) : null
            };

            return root;
        }
    }

    public class Node
    {
        public Node Left { get; set; }
        public Node Right { get; set; }
        public int Value { get; set; }

        public Node(int value)
        {
            Value = value;
        }

        public void PrintFromRoot()
        {
            PrintPretty(string.Empty, true);
        }

        private void PrintPretty(string indent, bool last)
        {
            Debug.Write(indent);
            if (last)
            {
                Debug.Write("\\-");
                indent += "  ";
            }
            else
            {
                Debug.Write("|-");
                indent += "| ";
            }
            Debug.WriteLine(Value);

            Left?.PrintPretty(indent, false);
            Right?.PrintPretty(indent, true);
        }
    }
}
