﻿namespace ArrayNumbers
{
    using FluentAssert;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class SortedMatrixSearch
    {
        [TestMethod]
        public void TestSearch()
        {
            var matrix = new int[][]
            {
                new[] {-1, 0, 2, 4, 8},
                new[] {1, 2, 4, 5, 9},
                new[] {2, 4, 5, 7, 10},
                new[] {3, 5, 6, 8, 11}
            };

            int column;
            var row = Search(matrix, 6, out column);
            row.ShouldBeEqualTo(3);
            column.ShouldBeEqualTo(2);

            row = Search(matrix, 11, out column);
            row.ShouldBeEqualTo(3);
            column.ShouldBeEqualTo(4);
        }


        public int Search(int[][] matrix, int el, out int column)
        {
            var iBound = 0;
            var jBound = 0;

            while (iBound < matrix.Length && jBound < matrix[0].Length && matrix[iBound][jBound] < el)
            {
                iBound++;
                jBound++;
            }

            var i = 0;
            var j = 0;
            var matrixHorisLength = matrix[0].Length;
            var matrixVertLength = matrix.Length;
            while (i < iBound && j < jBound)
            {
                // vertical
                if (matrix[i][j] <= el && matrix[matrixVertLength - 1][j] >= el)
                {
                    for (var tempI = i; tempI < matrixVertLength; tempI++)
                    {
                        if (matrix[tempI][j] == el)
                        {
                            column = j;
                            return tempI;
                        }
                    }
                }

                // horisontal
                if (matrix[i][j] <= el && matrix[i][matrixHorisLength - 1] >= el)
                {
                    for (var tempJ = j; tempJ < matrixHorisLength; tempJ++)
                    {
                        if (matrix[i][tempJ] == el)
                        {
                            column = tempJ;
                            return i;
                        }
                    }
                }

                i++;
                j++;
            }

            column = -1;
            return -1;
        }
    }    
}
