﻿namespace ArrayNumbers
{
    using FluentAssert;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    //Count the number of sequences of zeroes and ones of length n, which do not contain two subsequent ones (b) - tree subsequent ones
    [TestClass]
    public class FindCountSeq
    {
        [TestMethod]
        public void TestFindCount()
        {
            int count0 = 0;
            int count1 = 0;
            FindCount(0, 1, 4,ref count0);
            FindCount(1, 1, 4, ref count1);

            var result = count1 + count0;

            var result2 = FindCountSimpleСycle(4);
            result2.ShouldBeEqualTo(result);
        }

        private void FindCount(byte value, int currentLength, int n, ref int count)
        {
            if (currentLength == n)
            {
                count++;
            }
            else if (value == 0)
            {

                FindCount(1, currentLength + 1, n, ref count);
                FindCount(0, currentLength + 1, n, ref count);                
            }
            else
            {
                FindCount(0, currentLength + 1, n, ref count);
            }
        }

        private int FindCountSimpleСycle(int n)
        {
            var count0 = 1;
            var count1 = 1;
            for (int i = 1; i < n; i++)
            {
                var oldCount1 = count1;
                count1 = count0;
                count0 = oldCount1 + count0;                
            }

            return count1 + count0;
        }
    }
}
