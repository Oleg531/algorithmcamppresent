﻿namespace ArrayNumbers
{
    using FluentAssert;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class SplitArrayByX
    {
        [TestMethod]
        public void X_Povided_ShouldSplitArray()
        {
            var test = new[]
            {
                1, 4, 3, 7, 9, -1
            };
            var sorted = SortByX(test.Clone() as int[], 5);
            sorted.ShouldBeEqualTo(new[] {1, 4, 3, -1, 9, 7});

            test = new[]
            {
                4, 2, 7, 1, 6, 3, 0, 8
            };

            sorted = SortByX(test.Clone() as int[], 6);
            sorted.ShouldBeEqualTo(new[] {4, 2, 0, 1, 3, 6, 7, 8});
        }

        private int[] SortByX(int[] a, int x)
        {
            for (int i = 0; i < a.Length; i++)
            {
                if (a[i] >= x)
                {
                    for (int j = a.Length - 1; j > i; j--)
                    {
                        if (a[j] < x)
                        {
                            var temp = a[i];
                            a[i] = a[j];
                            a[j] = temp;
                        }
                    }
                }
            }
            return a;
        }
    }
}
