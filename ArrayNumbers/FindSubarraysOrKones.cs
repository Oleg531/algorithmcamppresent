﻿namespace ArrayNumbers
{
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    // Count the number of subarrays having OR with exact k ones
    [TestClass]
    public class FindSubarraysOrKones
    {
        [TestMethod]
        public void FindCountSubArrays_Test()
        {
            var count = FindCountSubArrays(new[] {5, 3, 11, 16}, 3);

        }

        public int FindCountSubArrays(int[] a, int k)
        {
            var bitArrays = a.Select(x => new BitArray(new [] {x})).ToArray();
            var countColumns = bitArrays.First().Count;
            var countRows = bitArrays.Count();

            var bitArraysInt = bitArrays.Select(ba => ba.Cast<bool>().Select(bit => bit ? 1 : 0).ToArray()).ToArray();
            var onesCountArray = new int[countRows][];
            for (var i = 0; i < countRows; i++)
            {
                onesCountArray[i] = new int[countColumns];
            }



            for (int j = 0; j < countColumns; j++)
            {
                var countOnes = 0;
                for (int i = 0; i < countRows; i++)
                {
                    if (bitArraysInt[i][j] == 1)
                    {
                        countOnes++;
                    }
                    onesCountArray[i][j] = countOnes;
                }
            }


            var countSubArrays = 0;
            for (int i = 0; i < countRows; i++)
            {
                var subArrayFound = false;
                var orArray = new int[countColumns];
                Array.Copy(onesCountArray[i], orArray, onesCountArray[i].Length);

                if (i > 0)
                {
                    for (int j = 0; j < countColumns; j++)
                    {
                        orArray[j] = orArray[j] - onesCountArray[i - 1][j];
                    }
                }

                for (int i2 = i; i2 < countRows; i2++)
                {
                    
                    for (int j = 0; j < countColumns; j++)
                    {
                        orArray[j] = orArray[j] | onesCountArray[i][j];
                    }

                    var countOnes = 0;
                    for (int j = 0; j < countColumns; j++)
                    {
                        if (orArray[j] > 0)
                        {
                            countOnes++;
                        }
                    }

                    if (countOnes == k)
                    {
                        subArrayFound = true;
                    }

                    if (countOnes > k)
                    {
                        break;
                    }
                }

                if (subArrayFound)
                {
                    countSubArrays++;
                }
            }

            return countSubArrays;
        }
    }
}
