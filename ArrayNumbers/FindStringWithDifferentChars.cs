﻿namespace ArrayNumbers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using FluentAssert;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class FindStringWithDifferentChars
    {
        [TestMethod]
        public void FindStringWithDifferentChars_Test()
        {
            FindString("abc", "xyc", 2).ShouldBeEqualTo("ayd");
            FindString("c", "b", 0).ShouldBeEqualTo(string.Empty);
        }

        private string FindString(string a, string b, int t)
        {
            if (a.Length != b.Length)
            {
                return string.Empty;
            }

            var strLength = a.Length;                        
            var resultString = new char[strLength];

            var takeSymbolFromFirstString = true;
            //first pass
            for (var i = 0; i < strLength; i++)
            {
                var firstChar = a[i];
                var secondChar = b[i];

                if (firstChar != secondChar)
                {
                    resultString[i] = takeSymbolFromFirstString ? firstChar : secondChar;
                    takeSymbolFromFirstString = !takeSymbolFromFirstString;
                }
            }

            //second pass
            //first pass
            for (var i = 0; i < strLength; i++)
            {
                var firstChar = a[i];
                var secondChar = b[i];

                if (firstChar != secondChar)
                {
                    resultString[i] = takeSymbolFromFirstString ? firstChar : secondChar;
                    takeSymbolFromFirstString = !takeSymbolFromFirstString;
                }
            }
            
            throw new NotImplementedException();

            //if (t == 0 && i < strLength || i == strLength && t > 0)
            //{
            //    return string.Empty;
            //}

            //return sb.ToString();
        }

        private char GenerateDifferentSymbol(char current)
        {
            return (char) (current + 1);
        }
    }
}
