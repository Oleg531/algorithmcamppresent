﻿namespace ArrayNumbers
{
    using System;
    using FluentAssert;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class FindKElementByIncreasesInArray
    {
        [TestMethod]
        public void ArrayProvided_ShouldFindElementByIncreases()
        {
            var testArray = new int[] {4, 8, 7, 1, 6, 3, 0, 2};
            var element = FindKElement((int[]) testArray.Clone(), 0, testArray.Length - 1, 5);
            element.ShouldBeEqualTo(4);            
        }

        private int FindKElement(int[] a, int startIndex, int endIndex, int k)
        {
            if (startIndex == endIndex)
            {
                return a[startIndex];
            }

            var middleIndex = (int) Math.Ceiling(((endIndex - startIndex)/2d)) + startIndex;
            SortByX(a, a[middleIndex], startIndex, endIndex, ref middleIndex);
            if (middleIndex - startIndex >= k)
            {
                return FindKElement(a, startIndex, middleIndex - 1, k);
            }

            return FindKElement(a, middleIndex, endIndex, k);
        }

        private void SortByX(int[] a, int x, int startIndex, int endIndex, ref int middleIndex)
        {
            for (int i = startIndex; i <= endIndex; i++)
            {
                if (a[i] >= x)
                {
                    for (int j = endIndex; j > i; j--)
                    {
                        if (a[j] < x)
                        {
                            var temp = a[i];
                            a[i] = a[j];
                            a[j] = temp;
                            if (i == middleIndex)
                            {
                                middleIndex = j;
                            }
                        }
                    }
                }
            }
        }
    }
}
