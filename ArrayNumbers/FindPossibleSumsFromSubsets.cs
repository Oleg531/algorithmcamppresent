﻿namespace ArrayNumbers
{
    using System.Collections.Generic;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    //Find all possible sums made from subsets of items from array A
    [TestClass]
    public class FindPossibleSumsFromSubsets
    {
        [TestMethod]
        public void FindAllPossibleSumsFromSubsets_ShouldFindSums()
        {
            var a = new[] {2, -1, 1, -2, 2};
            var sums = FindAllPossibleSumsFromSubsets(a);
        }

        private int[] FindAllPossibleSumsFromSubsets(int[] a)
        {
            if (a.Length == 0)
            {
                return new int[0];
            }

            var result = new List<int> {a[0]};

            for (var i = 0; i < a.Length; i++)
            {
                if (i < a.Length - 1)
                {                    
                    for (var k = i + 1; k < a.Length; k++)
                    {
                        var sum = a[i];
                        for (var j = k; j < a.Length; j++)
                        {
                            sum += a[j];
                        }
                        result.Add(sum);
                    }
                }
                else
                {
                    result.Add(a[i]);
                }
            }

            return result.ToArray();
        }
    }
}
