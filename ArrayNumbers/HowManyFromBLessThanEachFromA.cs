﻿namespace ArrayNumbers
{
    using System;
    using System.Collections.Generic;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class HowManyFromBLessThanEachFromA
    {
        [TestMethod]
        public void TwoArrays_FindHowManyElementsFromBLessThanA()
        {
            var a = new[] {2, 3, 8, 7, 6, 5, 4};
            var b = new[] {0, 2, 9, 5, 3, 22};

            var result = FindCountLessThanFirstArray(a, b);
        }

        private IEnumerable<KeyValuePair<int, int>> FindCountLessThanFirstArray(int[] a, int[] b)
        {
            //todo implement quick sort
            Array.Sort(a);
            Array.Sort(b);

            var i = 0;
            var j = 0;
            var result = new List<KeyValuePair<int,int>>();
            while (i < a.Length)
            {                
                while (b[j] < a[i])
                {
                    j++;
                }

                result.Add(new KeyValuePair<int, int>(a[i], j));
                i++;
            }


            return result;
        }



    }
}
