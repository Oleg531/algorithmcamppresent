﻿namespace ArrayNumbers
{
    using System;
    using System.Linq;
    using FluentAssert;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    //Леша не любит скучать.Поэтому, когда ему скучно, он придумывает игры.Как-то раз Леша придумал следующую игру.
    //Задана последовательность a, состоящая из n целых чисел.Игрок может сделать несколько ходов.
    //За один ход игрок может выбрать некоторый элемент последовательности (обозначим выбранный элемент ak) и удалить его, 
    //при этом из последователости также удаляются все элементы, равные ak + 1 и ak- 1. 
    //Описанный ход приносит игроку ak очков.
    //Леша максималист и поэтому хочет набрать как можно больше очков.Какое максимальное количество очков он сможет набрать?

    [TestClass]
    public class FindMaxScoreInArray
    {
        [TestMethod]
        public void Test_FindMaxScore()
        {            
            FindMaxScore(new[] { 1, 2, 1, 3, 2, 2, 2, 2, 3 }).ShouldBeEqualTo(10);
            FindMaxScore(new[] { 1, 2}).ShouldBeEqualTo(2);
            FindMaxScore(new[] { 1, 2, 3 }).ShouldBeEqualTo(4);
            FindMaxScore(new[] { 2, 2, 2 }).ShouldBeEqualTo(6);

            //2                        
            Iterate(new[] { 1, 2, 1, 3, 2, 2, 2, 2, 3 }).ShouldBeEqualTo(10);
            Iterate(new[] { 1, 2 }).ShouldBeEqualTo(2);
            Iterate(new[] { 1, 2, 3 }).ShouldBeEqualTo(4);
            Iterate(new[] { 2, 2, 2 }).ShouldBeEqualTo(6);

        }

        private int FindMaxScore(int[] inputArray)
        {
            if (inputArray.Length == 0)
            {
                return 0;
            }

            var a = inputArray.OrderByDescending(e => e).ToArray();

            var globalSum = 0;
            int? currentNumber = null;

            var sum1 = a[0];
            int? firstNumber = a[0];

            int? sum2 = null;
            int? secondNumber = null;
            for (int i = 1; i < a.Length; i++)
            {
                if (currentNumber.HasValue && Math.Abs(currentNumber.Value - a[i]) <= 1)
                {
                    continue;
                }

                if (!firstNumber.HasValue)
                {
                    firstNumber = a[i];
                }

                if (a[i] == firstNumber)
                {
                    sum1 += a[i];
                }
                else if (!secondNumber.HasValue)
                {
                    secondNumber = a[i];
                    sum2 = a[i];
                }
                else if (secondNumber.Value == a[i])
                {
                    sum2 += a[i];
                }
                else
                {
                    if (sum1 > sum2)
                    {
                        globalSum = sum1;
                        currentNumber = firstNumber;
                    }
                    else
                    {
                        globalSum = sum2.Value;
                        currentNumber = secondNumber.Value;
                    }

                    sum1 = 0;
                    sum2 = null;
                    firstNumber = null;
                    secondNumber = null;

                    if (Math.Abs(currentNumber.Value - a[i]) > 1)
                    {
                        sum1 = a[i];                        
                        firstNumber = a[i];                        
                    }
                }
            }

            globalSum += sum2.HasValue && sum2 > sum1 ? sum2.Value : sum1;

            return globalSum;

        }


        public int[] CreareSpecialArray(int[] a)
        {
            var max = a.Max();
            var countStoreArray = new int[max + 1];

            for (int i = 0; i < a.Length; i++)
            {
                countStoreArray[a[i]] = countStoreArray[a[i]] + 1;
            }

            return countStoreArray;
        }

        private int Iterate(int[] a)
        {
            var specArray = CreareSpecialArray(a);
            var fPrevPrev = specArray[1];
            var fPrev = specArray[2]*2;
            for (int i = 3; i < specArray.Length; i++)
            {
                var temp = fPrev;
                fPrev = Math.Max(fPrev, fPrevPrev + specArray[i]*i);
                fPrevPrev = temp;
            }

            return fPrev;
        }
    }
}