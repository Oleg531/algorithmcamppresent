﻿using System.Collections.Generic;

namespace ArrayNumbers
{
    using FluentAssert;
    using Microsoft.VisualStudio.TestTools.UnitTesting;


//http://codeforces.com/problemset/problem/474/D

//Мы видели маленькую игру, которую Сурок приготовил для Крота на обед.Теперь Сурку пора ужинать — а мы все знаем, что Сурок ест цветы. За каждым ужином он ест немного красных и немного белых цветов.Таким образом, ужин можно представить как последовательность белых и красных цветов.
//Но для того, чтобы ужин был вкусный, есть одно правило: когда Сурок хочет отведать белых цветов, он потребляет их только группами размера k.
//Теперь Сурку интересно, сколько существует способов съесть от a до b цветов по его правилам. Так как количество способов может быть очень большим, выведите его по модулю 1000000007 (109 + 7).
//Входные данные
//На вход подаётся несколько наборов входных данных.
//В первой строке записано два целых числа t и k(1 ≤ t, k≤ 105), где t обозначает количество тестовых примеров.
//В следующих t строках записано по два целых числа ai и bi (1 ≤ ai≤ bi≤ 105), описывающих i-й тест.



    [TestClass]
    public class FindFlavours
    {

        [TestMethod]
        public void FindCountCombinationsWhiteAndRedFlavours()
        {
            var max = 100000;
            var k = 2;
            var fDictionary = PrepareCountCombinationDictionary(k, max);
            var tDictionary = PepareSumCountCompositionsDictionary(max, fDictionary);

            FindCountCombination(1, 3, tDictionary).ShouldBeEqualTo(6);
            FindCountCombination(2, 3, tDictionary).ShouldBeEqualTo(5);
            FindCountCombination(4, 4, tDictionary).ShouldBeEqualTo(5);
        }

        private Dictionary<int, int> PrepareCountCombinationDictionary(int k, int max)
        {
            var fDictionary = new Dictionary<int, int>
            {
                {0, 1}
            };

            for (var i = 1; i < max; i++)
            {
                var x_k = i - k;
                var fx_1 = fDictionary[i - 1];
                var fx_k = x_k >= 0 ? fDictionary[x_k] : 0;

                fDictionary.Add(i, fx_1 + fx_k);
            }

            return fDictionary;
        }

        private Dictionary<int, int> PepareSumCountCompositionsDictionary(int max, Dictionary<int, int> fDictionary)
        {
            var tDictionary = new Dictionary<int, int>
            {
                {0, 1}
            };

            for (var i = 1; i < max; i++)
            {
                var tx_1 = tDictionary[i - 1];
                var fx = fDictionary[i];
                tDictionary.Add(i, tx_1 + fx);
            }

            return tDictionary;
        }

        private int FindCountCombination(int a, int b, Dictionary<int, int> tDictionary)
        {
            return tDictionary[b] - tDictionary[a - 1];
        }
    }
}
