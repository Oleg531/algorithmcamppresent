﻿namespace ArrayNumbers
{
    using FluentAssert;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    //You are given an array of positive and negative integers.
    //If a number n at an index is positive, then move forward n steps.Conversely, if it's negative, move backward n steps. 
    //Determine if there is a loop in this array. 
    [TestClass]
    public class FindLoopInArray
    {
        [TestMethod]
        public void FindLoop_ShouldDetectLoop()
        {
            var a = new int[] {2, -1, 1, -2, 2};
            var loopDetected = FindLoop(a);
            loopDetected.ShouldBeTrue();

            a = new[] {-1, 1};
            loopDetected = FindLoop(a);
            loopDetected.ShouldBeFalse();
        }

        private bool FindLoop(int[] a)
        {
            var slow = 0;
            var fast = 0;

            while (slow >= 0 && slow < a.Length && fast >= 0 && fast < a.Length)
            {
                slow = Jump(a, slow);
                fast = Jump(a, Jump(a, fast));                

                if (slow == fast && slow >= 0)
                {
                    return true;
                }
            }

            return false;
        }

        private int Jump(int[] a, int index)
        {
            var indexTemp = index >= 0 ? index + a[index] : -1;
            index = indexTemp >= 0 && indexTemp < a.Length ? indexTemp : -1;

            return index;
        }
    }
}
