using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace List
{
    using System.Diagnostics;
    using System.Text;

    // There is a cycled sorted linked list.The smaller element refers to the biggest one.
    // Implement the insert operation into the list. 
    // The insert operation must not brake the sort condition and the cycle condition.


   [TestClass]
    public class CycledLinkedListTask
    {
        [TestMethod]
        public void TestMethod1()
        {
            var data = new[] { 12, 56, 2, 11, 1, 90 };
            var list = new CycledLinkedList();
            foreach (var i in data)
            {
                list.Insert(i);
                list.Print();
            }            
        }
    }

    public class Node
    {
        public Node Next { get; set; }
        public int Value { get; }

        public Node(int value)
        {
            Value = value;
        }
    }

    public class CycledLinkedList
    {
        public Node Head { set; get; }

        public void Insert(int value)
        {
            // List is empty
            if (Head == null)
            {
                Head = new Node(value);
                Head.Next = Head;
            }
            // x < head -> new head with sycling saving 
            else if(value < Head.Value)
            {
                var last = Head;
                while (last.Next != Head)
                {
                    last = last.Next;
                }

                var newNode = new Node(value);
                newNode.Next = Head;
                last.Next = newNode;
                Head = newNode;
            }
            // insert x in list (min <= x <= max)
            else
            {
                var current = Head;
                while (current.Next.Value < value && current.Next != Head)
                {
                    current = current.Next;
                }

                var newNode = new Node(value);
                newNode.Next = current.Next;
                current.Next = newNode;                
            }
        }

        public void Print()
        {
            var output = new StringBuilder();
            if (Head == null)
            {
                return;
            }

            var current = Head;

            do
            {
                output.Append($"{current.Value}  -> ");
                current = current.Next;
            } while (current != Head);
            
            output.Append($"*");
            Debug.WriteLine(output.ToString());
        }
    }
}
