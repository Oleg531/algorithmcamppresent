﻿using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Sorting
{
    [TestClass]
    public class QuickSort
    {
        [TestMethod]
        public void Main()
        {
            var a = new[] { 2, 1, 4, 8, 6, 9, 5, 3 };
            quickSort(a, 0, a.Length - 1);
        }

        private void quickSort(int[] a, int startIndex, int endIndex)
        {
            //(l+r)/2
            var x = a[startIndex + (endIndex - startIndex) / 2];

            var i = startIndex;
            var j = endIndex;

            while (i <= j)
            {
                while (a[i] < x) i++;
                while (a[j] > x) j--;
                if (i <= j)
                {
                    var temp = a[i];
                    a[i] = a[j];
                    a[j] = temp;
                    i++;
                    j--;
                }
            }
            if (i < endIndex)
            {
                quickSort(a, i, endIndex);
            }

            if (startIndex < j)
            {
                quickSort(a, startIndex, j);
            }
        }
    }
}
