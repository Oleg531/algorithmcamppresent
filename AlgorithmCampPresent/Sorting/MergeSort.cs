﻿namespace Sorting
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class MergeSort
    {
        [TestMethod]
        public void MergeSort_ShouldSort()
        {
            var test = new [] {2, 3, 8, 7, 6, 5, 4};
            var sorted = Sort(test, 0, test.Length - 1);
        }

        public int[] Sort(int[] a, int startIndex, int endIndex)
        {
            if (endIndex - startIndex > 1)
            {
                var middleIndex = (int)Math.Ceiling(((endIndex - startIndex) / 2d));

                var partLeft = new int[middleIndex];
                Array.Copy(a,partLeft,middleIndex);

                var partRight = new int[endIndex - middleIndex + 1];
                Array.Copy(a, middleIndex, partRight, 0, endIndex - middleIndex + 1);

                var sortPartLeft = Sort(partLeft, 0, partLeft.Length - 1);
                var sortPartRight = Sort(partRight, 0, partRight.Length - 1);

                // merge 2 sorted arrays
                var j = 0;
                var k = 0;                                
                var mergeSortedArray = new int[sortPartLeft.Length + sortPartRight.Length];
                for (int i = 0; i < mergeSortedArray.Length; i++)
                {
                    if ((j < sortPartLeft.Length ? sortPartLeft[j] : int.MaxValue) <
                        (k < sortPartRight.Length ? sortPartRight[k] : int.MaxValue))
                    {
                        mergeSortedArray[i] = sortPartLeft[j];
                        j++;
                    }
                    else
                    {
                        mergeSortedArray[i] = sortPartRight[k];
                        k++;
                    }
                }

                return mergeSortedArray;
            }

            var part = new int[endIndex - startIndex + 1];
            if (endIndex == startIndex)
            {
                part[0] = a[startIndex];
            }
            else
            {
                part[0] = a[startIndex] < a[endIndex] ? a[startIndex] : a[endIndex];
                part[1] = a[startIndex] < a[endIndex] ? a[endIndex] : a[startIndex];
            }

            return part;            
        }
    }
}
