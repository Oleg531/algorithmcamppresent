﻿using System.Linq;

namespace Strings
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    // Вывести слова в строке в обратном порядке
    [TestClass]
    public class ReverseStringWithWords
    {
        [TestMethod]
        public void Test()
        {
            var ex = "Hello mom how are you".ToArray();
            ReverseWords(ex);
            var str = string.Join("", ex);
            Assert.AreEqual("you are how mom Hello",str);
        }

        private void Reverse(char[] a, int start, int finish)
        {
            int countMax = (finish - start) / 2;

            var count = 0;
            var i = start;
            var j = finish;

            while (count < countMax)
            {
                var temp = a[i];
                a[i] = a[j];
                a[j] = temp;
                i++;
                j--;
                count++;
            }
        }

        public void ReverseWords(char[] a)
        {
            Reverse(a,0,a.Length-1);
            var i = 0;
            while (i < a.Length)
            {
                var k = i;
                while (k < a.Length && a[k] != ' ')
                {
                    k++;
                }
                Reverse(a,i,k - 1);
                i = k + 1;
            }
        }
    }
}
