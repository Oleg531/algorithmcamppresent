﻿namespace Strings
{
    using System;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    // 1)reverse the array of chars
    // 2)same question but one character lies in two cells of the array
    // 3)same question but array contains simple characters and characters lies in two cells
    //(we have function int getCharLength(char firstCharacter))    
    [TestClass]
    public class ReverseArrayOfCharsBetterSolution
    {
        [TestMethod]
        public void Test()
        {
            // 1)reverse the array of chars
            var ex1 = new char[] {'a', 'b', 'c', 'd', 'e', 'f'};
            Reverse(ex1, 0, ex1.Length - 1);
            Assert.AreEqual("fedcba", string.Join("",ex1));

            ex1 = new char[] { 'a', 'b', 'c', 'd', 'e' };
            Reverse(ex1, 0, ex1.Length - 1);
            Assert.AreEqual("edcba", string.Join("", ex1));

            // 2)same question but one character lies in two cells of the array
            ex1 = new char[] {'a', 'b', 'c', 'd', 'e', 'f'};
            ReverseWords(ex1);
            Reverse(ex1, 0, ex1.Length - 1);
            Assert.AreEqual("efcdab", string.Join("", ex1));

            // 3)same question but array contains simple characters 
            //and characters lies in two cells
            //(we have function int getCharLength(char firstCharacter))
            ex1 = new char[] { 'A', 'b', 'c', 'D', 'e', 'f' };
            ReverseWords(ex1, GetWordLength);
            Reverse(ex1, 0, ex1.Length - 1);
            Assert.AreEqual("fDecAb", string.Join("", ex1));
        }

        private void ReverseWords(char[] a, Func<char, int> getWordLength = null)
        {
            var i = 0;
            while (i < a.Length)
            {
                var k = i + (getWordLength?.Invoke(a[i]) ?? 2) - 1; // 2 is word's length
                Reverse(a, i, k);
                i = k + 1;
            }
        }

        private void Reverse(char[] a, int start, int finish)
        {
            int countMax = (finish + 1 - start) / 2;

            var count = 0;
            var i = start;
            var j = finish;

            while (count < countMax)
            {
                var temp = a[i];
                a[i] = a[j];
                a[j] = temp;
                i++;
                j--;
                count++;
            }
        }        

        private int GetWordLength(char c)
        {
            return c >= 'A' && c <= 'Z' ? 2 : 1;
        }
    }
}
