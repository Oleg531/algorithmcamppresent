﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Strings
{
    using System.Collections.Generic;
    using System.Linq;

    // 1)reverse the array of chars
    // 2)same question but one character lies in two cells of the array
    // 3)same question but array contains simple characters and characters lies in two cells
    //(we have function int getCharLength(char firstCharacter))    
    [TestClass]
    public class ReverseArrayOfChars
    {
        [TestMethod]
        public void TestMethod1()
        {            
            var ex = new List<char> {'a', 'b', 'c', 'd', 'e'};

            // test 1
            var res1 = Reverse1(ex.ToArray());
            ex.Reverse();
            Assert.IsTrue(string.Join("", ex) == string.Join("", res1));


            ex.Reverse();
            ex.Add('f');
            res1 = Reverse1(ex.ToArray());
            ex.Reverse();
            Assert.IsTrue(string.Join("", ex) == string.Join("", res1));

            // test 2
            ex.Reverse();
            res1 = Reverse2(ex.ToArray());
            Assert.IsTrue("efcdab" == string.Join("", res1));

            // test3
            var ex3 = new[] {'A','b','c','D','e','f'};
            res1 = Reverse3(ex3);
            Assert.IsTrue("fDecAb" == string.Join("", res1));
        }

        // 1)reverse the array of chars
        public char[] Reverse1(char[] arr)
        {
            // ReSharper disable once SuggestVarOrType_BuiltInTypes
            int countMax = arr.Length / 2;
            var count = 0;
            var i = 0;
            var j = arr.Length - 1;

            while (count < countMax)
            {
                var temp = arr[i];
                arr[i] = arr[j];
                arr[j] = temp;
                i++;
                j--;
                count++;
            }

            return arr;
        }

        // 2)same question but one character lies in two cells of the array
        public char[] Reverse2(char[] arr)
        {
            // ReSharper disable once SuggestVarOrType_BuiltInTypes
            int countMax = arr.Length / 4;
            var count = 0;
            var i = 0;
            var j = arr.Length - 1;

            while (count < countMax)
            {
                var temp = arr[i];
                arr[i] = arr[j -1];
                arr[j - 1] = temp;
                temp = arr[i + 1];
                arr[i + 1] = arr[j];
                arr[j] = temp;
                i+=2;
                j-=2;
                count++;
            }

            return arr;
        }

        // 3)same question but array contains simple characters and characters lies in two cells
        public char[] Reverse3(char[] arr)
        {
            var s = new Stack<char>();
            var i = 0;
            while (i < arr.Length)
            {
                var l = GetWordLength(arr[i]);
                var k = i + l - 1;
                var temp = k;
                while (temp >= i)
                {
                    s.Push(arr[temp]);
                    temp--;
                }

                i = k + 1;
            }

            for (var n = 0; n < arr.Length; n++)
            {
                arr[n] = s.Pop();
            }

            return arr;
        }

        private int GetWordLength(char c)
        {
            return c >= 'A' && c <= 'Z' ? 2 : 1;
        }


    }
}
